package main.java.dao;

import java.util.List;

import main.java.model.Image;
import main.java.model.Product;

public interface ProductDAO {

	List<Product> getProductsWithoutRelationships();
	
	List<Product> getProductsWithRelationships();
	
	List<Product> getProductsWithoutRelationshipsByProduct(long id);
	
	List<Product> getProductsWithRelationshipsByProduct(long id);
	
	List<Product> getSetOfChildProductsByProduct(long id);
	
	List<Image> getSetOfImagesByProduct(long id);

	void insertProduct(Product product);
}
