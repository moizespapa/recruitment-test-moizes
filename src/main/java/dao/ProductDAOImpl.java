package main.java.dao;

import java.util.List;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import main.java.model.Image;
import main.java.model.Product;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public class ProductDAOImpl implements ProductDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	//@PersistenceContext protected EntityManager entityManager;
	
	@Resource public EntityManagerFactory factory;
	
	@Resource public UserTransaction utx;

	@Transactional(readOnly = false)
	public void insertProduct(Product product) {
		
		EntityManager em = factory.createEntityManager();
		
		try {
			em.persist(product);
		}
		catch (RuntimeException e) {
		    throw e; // or display error message
		}
		finally {
		    em.close();
		}
	}

	public List<Product> getProductsWithoutRelationships() {
		Session session = sessionFactory.openSession();
		String hql = "FROM PRODUCT";
		Query query = session.createQuery(hql);
		List<Product> products = query.list();
		return products;
	}

	public List<Product> getProductsWithRelationships() {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Product> getProductsWithoutRelationshipsByProduct(long id) {
		Session session = sessionFactory.openSession();
		String hql = "FROM PRODUCT WHERE PRODUCT.ID = " + id;
		Query query = session.createQuery(hql);
		List<Product> products = query.list();
		return products;
	}

	@Override
	public List<Product> getProductsWithRelationshipsByProduct(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Product> getSetOfChildProductsByProduct(long id) {
		Session session = sessionFactory.openSession();
		String hql = "FROM PRODUCT WHERE PRODUCT.PARENT_PRODUCT_ID = " + id;
		Query query = session.createQuery(hql);
		List<Product> products = query.list();
		return products;
	}

	public List<Image> getSetOfImagesByProduct(long id) {
		Session session = sessionFactory.openSession();
		String hql = "FROM IMAGE WHERE PRODUCT.PARENT_PRODUCT_ID = " + id;
		Query query = session.createQuery(hql);
		List<Image> images = query.list();
		return images;
	}
}
