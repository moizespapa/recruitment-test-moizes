package main.java.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Entity
@Table(name = "PRODUCT")
public class Product  implements Serializable {

	private static final long serialVersionUID = 5910314716260013976L;

	public Product() {

	}

	public Product(String name, String description) {

		this.name = name;
		this.description = description;
	}

	public Product(String name, String description, Product parent_product_id) {
		super();
		this.name = name;
		this.description = description;
		this.parent_product_id = parent_product_id;
	}

	public Product(String name, String description, Product parent_product_id,
			List<Image> images) {
		super();
		this.name = name;
		this.description = description;
		this.parent_product_id = parent_product_id;
		this.images = images;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", unique = true, nullable = false)
	private Long id;

	@Getter
	@Setter
	@Column(name = "NAME", nullable = false)
	private String name;

	@Getter
	@Setter
	@Column(name = "DESCRIPTION", nullable = false)
	private String description;

	@Getter
	@Setter
	@Column(name = "PARENT_PRODUCT_ID")
	private Product parent_product_id;
	
	@Getter
	@Setter
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "product_id")
	private List<Image> images = new ArrayList<Image>();

}
