package main.java.service;

import java.util.List;

import main.java.model.Image;
import main.java.model.Product;

public interface ProductService {
	
	List<Product> getProductsWithoutRelationships();
	
	List<Product> getProductsWithRelationships();
	
	List<Product> getProductsWithoutRelationshipsByProduct(Long id);
	
	List<Product> getProductsWithRelationshipsByProduct(Long id);
	
	List<Product> getSetOfChildProductsByProduct(Long id);
	
	List<Image> getSetOfImagesByProduct(Long id);
	
	void insertProduct(Product product);

}
