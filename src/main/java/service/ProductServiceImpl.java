package main.java.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import main.java.dao.ProductDAO;
import main.java.model.Image;
import main.java.model.Product;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired ProductDAO productdao;

	public List<Product> getProductsWithoutRelationships(){
		return productdao.getProductsWithoutRelationships();
	}

	public List<Product> getProductsWithRelationships(){
		return productdao.getProductsWithRelationships();
	}

	public List<Product> getProductsWithoutRelationshipsByProduct(Long id){
		List<Product> result =  new ArrayList<Product>();
		if(id != null){
			result = productdao.getProductsWithoutRelationshipsByProduct(id);
		}
		return result;
	}

	public List<Product> getProductsWithRelationshipsByProduct(Long id){
		List<Product> result =  new ArrayList<Product>();
		if(id != null){
			result = productdao.getProductsWithRelationshipsByProduct(id);
		}
		return result;
	}


	public List<Product> getSetOfChildProductsByProduct(Long id){
		List<Product> result =  new ArrayList<Product>();
		if(id != null){
			result = productdao.getSetOfChildProductsByProduct(id);
		}
		return result;
	}

	public List<Image> getSetOfImagesByProduct(Long id){
		List<Image> result =  new ArrayList<Image>();
		if(id != null){
			result = productdao.getSetOfImagesByProduct(id);
		}
		return result;
	}

	public void insertProduct(Product product) {
		productdao.insertProduct(product);
	}

}
