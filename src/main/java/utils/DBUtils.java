package main.java.utils;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DBUtils {

	@Autowired
	private DataSource dataSource;
	
	@PostConstruct
	public void initialize(){
		try {
			Connection connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			statement.execute("DROP TABLE IF EXISTS PRODUCT");
			statement.executeUpdate(
					"CREATE TABLE PRODUCT(" +
					"ID INTEGER Primary key, " +
					"NAME varchar(30) not null, " +
					"DESCRIPTION varchar(30) not null)"
					);
			statement.executeUpdate(
					"INSERT INTO PRODUCT " +
					"(NAME, DESCRIPTION) " +
					"VALUES " + "('DONALD', 'TRUMP')"
					);
			statement.close();
			connection.close();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
