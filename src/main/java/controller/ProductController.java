package main.java.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import main.java.model.Product;
import main.java.service.ProductServiceImpl;

@Path("/products")
public class ProductController {

	ProductServiceImpl service = new ProductServiceImpl();

	@GET
	@Path("/withoutrelationships")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getProductsWithoutRelationships()
	{
		List<Product> productList = service.getProductsWithoutRelationships();
		return productList;
	}
	
    @GET
    @Path("/withrelationships")
    @Produces(MediaType.APPLICATION_JSON)
	public List<Product> getProductsWithRelationships()
	{
		return service.getProductsWithRelationships();
	}
    
    @GET
    @Path("/withoutrelationships/{id}")
    @Produces(MediaType.APPLICATION_JSON)
	public List<Product> getProductWithoutRelationshipsById(@PathParam("id") long id)
	{
		return service.getProductsWithoutRelationshipsByProduct(id);
	}
    
    @GET
    @Path("/withrelationships/{id}")
    @Produces(MediaType.APPLICATION_JSON)
	public List<Product> getProductsWithRelationshipsByProduct(@PathParam("id") long id)
	{
		return service.getProductsWithRelationshipsByProduct(id);
	}
    
    @GET
    @Path("/setofchildproducts/{id}")
    @Produces(MediaType.APPLICATION_JSON)
	public List<Product> getSetOfChildProductsByProduct(@PathParam("id") long id)
	{
		return service.getSetOfChildProductsByProduct(id);
	}
    
    @GET
    @Path("/setofimages/{id}")
    @Produces(MediaType.APPLICATION_JSON)
	public List<Product> getSetOfImagesByProduct(@PathParam("id") long id)
	{
		return service.getProductsWithRelationshipsByProduct(id);
	}

}
