package main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import main.java.model.Image;
import main.java.model.Product;
import main.java.service.ProductService;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main (String[] args) throws Exception
	{
		
//		Connection c = null;
//		try {
//			Class.forName("org.sqlite.JDBC");
//			c = DriverManager.getConnection("jdbc:sqlite:recruitment-test-moizes.db");
//
//		} catch ( Exception e ) {
//			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
//			System.exit(0);
//		}
//
//		System.out.println("Opened database successfully");
//		
//		String sDropTableProduct= "DROP TABLE IF EXISTS PRODUCT ";
//		String sMakeProduct= "CREATE TABLE PRODUCT (ID numeric NOT NULL, NAME text NOT NULL, DESCRIPTION text NOT NULL, PARENT_PRODUCT_ID numeric)";
//		String sMakeInsertProduct = "INSERT INTO PRODUCT VALUES(1,'FIRST PRODUCT', 'DESCRIPTION', NULL)";
//		String sDropTableImage= "DROP TABLE IF EXISTS IMAGE ";
//		String sMakeImage= "CREATE TABLE IMAGE (ID numeric NOT NULL, TYPE BLOB, PRODUCT_ID numeric)";
//		String sMakeInsertImage = "INSERT INTO IMAGE VALUES(1,NULL,1)";
//
//		String sMakeSelect = "SELECT * from PRODUCT";
//		
//		EntityManagerFactory emf =
//			      Persistence.createEntityManagerFactory("recruitment-test-moizes");
//		
//		EntityManager em = emf.createEntityManager();
//		  try {
//			  Product example = new Product("Name", "Description");
//			  Image image = new Image(null, example);
//			  em.getTransaction().begin();
//			  em.persist(example);
//			  em.getTransaction().commit();
//		  }
//		  finally {
//		      em.close();
//		  }
//
//
//		try {
//
//			Statement stmt = c.createStatement();
//
//			try {
//
//				stmt.setQueryTimeout(30);
//				stmt.executeUpdate( sDropTableImage );
//				stmt.executeUpdate( sDropTableProduct );
//
//				stmt.executeUpdate( sMakeProduct );
//				stmt.executeUpdate( sMakeImage );
//
//				//stmt.executeUpdate( sMakeInsertProduct );
//				//stmt.executeUpdate( sMakeInsertImage );
//				
//
//				ResultSet rs = stmt.executeQuery(sMakeSelect);
//
//				try {
//
//					while(rs.next())
//
//					{
//
//						String sResult = rs.getString("DESCRIPTION");
//
//						System.out.println(sResult);
//
//					}
//
//				} finally {
//
//					try { rs.close(); } catch (Exception ignore) {}
//
//				}
//
//			} finally {
//
//				try { stmt.close(); } catch (Exception ignore) {}
//
//			}
//
//		} finally {
//
//			try { c.close(); } catch (Exception ignore) {}
//
//		}
//	}

		ApplicationContext context = 
				new ClassPathXmlApplicationContext(
						"/META-INF/spring/app-context.xml");
		ProductService productService = 
				context.getBean("productServiceImpl", ProductService.class);
		
		Product product = new Product("product", "description");
		
		

		productService.insertProduct(product);
		
	}

}
